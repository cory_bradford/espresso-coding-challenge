RecyclerView Espresso Coding Challenge
============================

This coding challenge solution was built on top of the template code for RecyclerView as described
in the challenge description.

Template code I used:
https://github.com/google-developer-training/android-fundamentals-apps-v2/tree/master/RecyclerView

